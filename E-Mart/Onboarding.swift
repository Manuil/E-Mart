//
//  Onboarding.swift
//  E-Mart
//
//  Created by bnkwsr8 on 09.06.2023.
//

import SwiftUI

struct Onboarding: View {
    @Binding var screen: String
    var body: some View {
        ZStack {
            Image("BGOB")
                .resizable()
                .scaledToFill()
                .ignoresSafeArea()
            VStack {
                HStack {
                    Spacer()
                    Button {
                        screen = "Login"
                    } label: {
                        Text("Skip")
                            .foregroundColor(Color("Text"))
                            .font(.custom("SourceSansPro-Regular", size: 18))
                    }
                    .padding(.top, 50)
                    .padding(.horizontal, 20)
                }
                Spacer()
                VStack(spacing: 0) {
                    Text("Get your groceries delivered to your home")
                        .font(.custom("SourceSansPro-Bold", size: 28))
                        .padding(.horizontal, 36)
                        .multilineTextAlignment(.center)
                        .foregroundColor(Color("Text"))
                    Text("The best delivery app in town for delivering your daily fresh groceries")
                        .padding(.top, 20)
                        .padding(.horizontal, 58)
                        .font(.custom("SourceSansPro-SemiBold", size: 16))
                        .multilineTextAlignment(.center)
                        .foregroundColor(Color("TextLight"))
                    Image("Onboard")
                        .resizable()
                        .scaledToFit()
                        .frame(width: UIScreen.main.bounds.width - 160)
                        .padding(.top, 70)
                }
                Spacer()
                HStack {
                    ForEach(0...4, id: \.self) { id in
                        if id == 2 {
                            Circle()
                                .frame(width: 17)
                                .foregroundColor(Color("Orange"))
                        } else {
                            Circle()
                                .frame(width: 14)
                                .foregroundColor(.gray.opacity(0.2))
                        }
                    }
                }
                .padding(.bottom, 40)
            }
        }
    }
}
