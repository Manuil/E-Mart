//
//  SplashScreen.swift
//  E-Mart
//
//  Created by bnkwsr8 on 09.06.2023.
//

import SwiftUI

struct SplashScreen: View {
    @State var alert = false
    @StateObject var connect = NetworkMonitor()
    @Binding var screen: String
    var body: some View {
        VStack {
            Spacer()
            Spacer()
            Image("Logo")
                .resizable()
                .scaledToFit()
                .frame(width: UIScreen.main.bounds.width - 170)
            Spacer()
            Spacer()
            Text("E-Mart")
                .font(.custom("SourceSansPro-Bold", size: 50))
                .foregroundColor(.white)
            Spacer()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color("BGSS"))
        .onAppear {
            connect.checkInternetConnection()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if connect.isConnected {
                    screen = "Onboarding"
                } else {
                    alert = true
                }
            }
            for fontFamily in UIFont.familyNames {
                print(fontFamily)
                for font in UIFont.fontNames(forFamilyName: fontFamily) {
                    print("-\(font)")
                }
            }
        }
        .alert(isPresented: $alert) {
            Alert(title: Text("Ошибка"), message: Text("Отсутствует подключение к интернету"), dismissButton: .default(Text("Обновить"), action: refresh))
        }
    }
    func refresh() {
        connect.checkInternetConnection()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if connect.isConnected {
                screen = "Onboarding"
            } else {
                alert = true
            }
        }
    }
}
