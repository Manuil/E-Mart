//
//  E_MartApp.swift
//  E-Mart
//
//  Created by bnkwsr8 on 09.06.2023.
//

import SwiftUI

@main
struct E_MartApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
