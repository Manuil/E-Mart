//
//  MapRoute.swift
//  E-Mart
//
//  Created by bnkwsr8 on 13.06.2023.
//

import SwiftUI
import MapKit

struct MapRoute: View {
    @State private var sourceCoordinate: CLLocationCoordinate2D?
    @State private var destinationCoordinate: CLLocationCoordinate2D?
    @State private var route: MKRoute?
    
    var body: some View {
        VStack {
            MapView(sourceCoordinate: $sourceCoordinate, destinationCoordinate: $destinationCoordinate, route: $route)
                .edgesIgnoringSafeArea(.all)
                .onAppear {
                    let sourcePlacemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 53.656291, longitude: 52.435193)) // Координаты исходного места
                    let destinationPlacemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 53.662801, longitude: 52.424162)) // Координаты пункта назначения
                    
                    let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
                    let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
                    
                    let directionRequest = MKDirections.Request()
                    directionRequest.source = sourceMapItem
                    directionRequest.destination = destinationMapItem
                    directionRequest.transportType = .walking // Тип транспорта (автомобиль, пешком и т.д.)
                    
                    let directions = MKDirections(request: directionRequest)
                    directions.calculate { (response, error) in
                        guard let route = response?.routes.first else {
                            return
                        }
                        self.route = route
                    }
                }
            
            Text("Route Distance: \(route?.distance ?? 0)m")
                .padding()
        }
    }
}

struct MapView: UIViewRepresentable {
    @Binding var sourceCoordinate: CLLocationCoordinate2D?
    @Binding var destinationCoordinate: CLLocationCoordinate2D?
    @Binding var route: MKRoute?
    
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView()
        mapView.delegate = context.coordinator
        return mapView
    }
    
    func updateUIView(_ view: MKMapView, context: Context) {
        view.removeAnnotations(view.annotations)
        
        if let sourceCoordinate = sourceCoordinate {
            let sourceAnnotation = MKPointAnnotation()
            sourceAnnotation.coordinate = sourceCoordinate
            view.addAnnotation(sourceAnnotation)
        }
        
        if let destinationCoordinate = destinationCoordinate {
            let destinationAnnotation = MKPointAnnotation()
            destinationAnnotation.coordinate = destinationCoordinate
            view.addAnnotation(destinationAnnotation)
        }
        
        if let route = route {
            view.removeOverlays(view.overlays)
            view.addOverlay(route.polyline)
            view.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, MKMapViewDelegate {
        var parent: MapView
        
        init(_ parent: MapView) {
            self.parent = parent
        }
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            if let polyline = overlay as? MKPolyline {
                let renderer = MKPolylineRenderer(overlay: polyline)
                renderer.strokeColor = UIColor(Color("Orange"))
                renderer.lineWidth = 5
                return renderer
            }
            return MKOverlayRenderer()
        }
    }
}

struct MapRoute_Previews: PreviewProvider {
    static var previews: some View {
        MapRoute()
    }
}
