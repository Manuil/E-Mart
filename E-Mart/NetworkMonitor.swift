//
//  NetworkManager.swift
//  E-Mart
//
//  Created by bnkwsr8 on 19.06.2023.
//

import Foundation
import Network

class NetworkMonitor: ObservableObject {
    @Published var isConnected = true
    
    func checkInternetConnection() {
        let monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "NetworkMonitor")
        
        monitor.pathUpdateHandler = { path in
            DispatchQueue.main.async {
                if path.status == .satisfied {
                    self.isConnected = true
                } else {
                    self.isConnected = false
                }
            }
        }
        
        monitor.start(queue: queue)
    }
}

