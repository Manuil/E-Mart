//
//  OTP.swift
//  E-Mart
//
//  Created by bnkwsr8 on 13.06.2023.
//

import SwiftUI

struct OTP: View {
    @StateObject var connect = NetworkMonitor()
    @StateObject var loginView = LoginViewModel()
    @State var alert = false
    private let maxLength = 4
    var body: some View {
        ZStack {
            VStack {
                Image("BGLogin")
                    .resizable()
                    .scaledToFit()
                    .ignoresSafeArea()
                    .offset(y: -200)
                Spacer()
            }
            VStack(spacing: 0) {
                Text("OTP Verification")
                    .font(.custom("SourceSansPro-Bold", size: 28))
                    .foregroundColor(Color("Text"))
                Text("Enter the OTP which is send to your mailing\naddress : \(UserDefaults.standard.string(forKey: "email")!)")
                    .multilineTextAlignment(.center)
                    .font(.custom("SourceSansPro-SemiBold", size: 14))
                    .foregroundColor(Color("TextDark"))
                    .padding(.top, 30)
                HStack(alignment: .center) {
                    Spacer()
                    TextField("", text: $loginView.code)
                        .keyboardType(.numberPad)
                        .font(.custom("SourceSansPro-SemiBold", size: 24))
                        .kerning(41)
                        .onChange(of: loginView.code) { newValue in
                            if newValue.count > maxLength {
                                loginView.code = String(newValue.prefix(maxLength))
                            }
                        }
                        .frame(width: 250)
                        .offset(x: 38)
                    Spacer()
                }
                .padding(.top, 50)
                HStack(spacing: 15) {
                    ForEach(0...3, id: \.self) { _ in
                        Rectangle()
                            .frame(width: 38.5, height: 1)
                            .foregroundColor(Color("TextDark"))
                    }
                }
                .padding(.top, 5)
                HStack(spacing: 4) {
                    Text("Don’t received OTP?")
                        .foregroundColor(Color("TextDark"))
                    Button {
                        loginView.email = UserDefaults.standard.string(forKey: "email")!
                        connect.checkInternetConnection()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            if connect.isConnected {
                                loginView.postSendCode {
                                    //
                                } error: {
                                    alert = true
                                }
                            } else {
                                loginView.loginError = "Отсутствует подключение к интернету"
                                alert = true
                            }
                        }
                    } label: {
                        Text("Resend OTP")
                            .foregroundColor(Color("Orange"))
                    }
                }
                .font(.custom("SourceSansPro-SemiBold", size: 14))
                .padding(.top, 30)
                Button {
                    connect.checkInternetConnection()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        if connect.isConnected {
                            loginView.postSignIn {
                                UserDefaults.standard.set(loginView.token, forKey: "token")
                                debugPrint("Успешно")
                                debugPrint(UserDefaults.standard.string(forKey: "token")!)
                            } error: {
                                alert = true
                            }
                        } else {
                            loginView.loginError = "Отсутствует подключение к интернету"
                            alert = true
                        }
                    }
                } label: {
                    Text("Verify & Preceed")
                        .font(.custom("SourceSansPro-SemiBold", size: 16))
                        .foregroundColor(.white)
                        .padding(16)
                        .frame(maxWidth: .infinity)
                        .background(Color("Orange"))
                        .cornerRadius(100)
                        .padding(.horizontal, 40)
                }
                .padding(.top, 50)
                Spacer()
            }
            .padding(.top, 50)
        }
        .ignoresSafeArea(.keyboard)
        .alert(isPresented: $alert) {
            Alert(title: Text("Ошибка"), message: Text(loginView.loginError))
        }
    }
}

struct OTP_Previews: PreviewProvider {
    static var previews: some View {
        OTP()
    }
}
