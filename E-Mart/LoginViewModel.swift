//
//  ApiView.swift
//  E-Mart
//
//  Created by bnkwsr8 on 19.06.2023.
//

import Foundation
import Alamofire

class LoginViewModel: ObservableObject {
    @Published var email = ""
    @Published var code = ""
    @Published var token = ""
    @Published var loginError = ""
    
    private let link = "https://medic.madskill.ru"
    
    func postSendCode(success: @escaping () -> Void, error: @escaping () -> Void) {
        if email.isEmpty {
            loginError = "Введите значение"
            error()
            return()
        }
        let emailMask = "[a-z0-9]+@[a-z0-9]+\\.[a-z]{1,3}"
        let emailCheck = NSPredicate(format: "SELF MATCHES %@", emailMask)
        let isCorrectEmail = emailCheck.evaluate(with: email)
        if !isCorrectEmail {
            loginError = "Неверно введен почтовый адрес"
            error()
            return()
        }
        var headers = HTTPHeaders()
        headers.add(name: "email", value: UserDefaults.standard.string(forKey: "email")!)
        AF.request("\(link)/api/sendCode", method: .post, headers: headers)
            .response { res in
                if res.data != nil {
                    debugPrint(res)
                    success()
                } else {
                    self.loginError = "Ошибка сервера"
                    error()
                }
            }
    }
    
    func postSignIn(success: @escaping () -> Void, error: @escaping () -> Void) {
        if code.count < 4 {
            loginError = "Введите значение"
            error()
            return()
        }
        var headers = HTTPHeaders()
        headers.add(name: "email", value: UserDefaults.standard.string(forKey: "email")!)
        headers.add(name: "code", value: code)
        AF.request("\(link)/api/signin", method: .post, headers: headers)
            .responseDecodable(of: SignIn.self) { res in
                if res.data != nil {
                    if let resToken = res.value?.token {
                        debugPrint(res)
                        self.token = resToken
                        success()
                    } else {
                        self.loginError = "Неверный код"
                        error()
                    }
                } else {
                    self.loginError = "Ошибка сервера"
                    error()
                }
            }
    }
}
struct SignIn: Codable {
    let token: String
}
