//
//  ContentView.swift
//  E-Mart
//
//  Created by bnkwsr8 on 09.06.2023.
//

import SwiftUI

struct ContentView: View {
    @State var screen = "SplashScreen"
    var body: some View {
        VStack {
            if screen == "SplashScreen" {
                SplashScreen(screen: $screen)
            } else if screen == "Onboarding" {
                Onboarding(screen: $screen)
            } else if screen == "Login" {
                Login(screen: $screen)
            } else if screen == "OTP" {
                OTP()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
