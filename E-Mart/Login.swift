//
//  Login.swift
//  E-Mart
//
//  Created by bnkwsr8 on 09.06.2023.
//

import SwiftUI

struct Login: View {
    @StateObject var connect = NetworkMonitor()
    @StateObject var loginView = LoginViewModel()
    @Binding var screen: String
    @State var userName = ""
    @FocusState var userNameFocused: Bool
    @State private var password = ""
    @FocusState var passwordFocused: Bool
    @FocusState var emailFocused: Bool
    @State var confirmPassword = ""
    @FocusState var confirmPasswordFocused: Bool
    @State var login = true
    @State var check = false
    @State var alert = false
    var body: some View {
        ZStack {
            VStack {
                Image("BGLogin")
                    .resizable()
                    .scaledToFit()
                    .ignoresSafeArea()
                    .offset(y: login ? -200 : 0)
                Spacer()
            }
            VStack(spacing: 0) {
                Text(login ? "Login" : "Signup")
                    .font(.custom("SourceSansPro-Bold", size: 28))
                    .foregroundColor(Color("Text"))
                    .padding(.top, 80)
                VStack(spacing: 0) {
                    HStack(spacing: 0) {
                        Image("User")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 24)
                            .padding(.leading, 15)
                        ZStack {
                            HStack {
                                if userNameFocused == false && userName.isEmpty {
                                    Text(login ? "User name" : "Full name")
                                        .font(.custom("SourceSansPro-SemiBold", size: 16))
                                        .foregroundColor(.black)
                                }
                                Spacer()
                            }
                            .padding(.leading, 15)
                            TextField("", text: $userName)
                                .focused($userNameFocused)
                                .padding(.horizontal, 15)
                                .font(.custom("SourceSansPro-SemiBold", size: 16))
                                .foregroundColor(.black)
                        }
                    }
                    .frame(maxWidth: .infinity)
                    .frame(height: 52)
                    .background(.white)
                    .cornerRadius(30)
                    .padding(.horizontal, 40)
                    if login == false {
                        HStack(spacing: 0) {
                            Image("Email")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 24)
                                .padding(.leading, 15)
                            ZStack {
                                HStack {
                                    if emailFocused == false && loginView.email.isEmpty {
                                        Text("Email")
                                            .font(.custom("SourceSansPro-SemiBold", size: 16))
                                            .foregroundColor(.black)
                                    }
                                    Spacer()
                                }
                                .padding(.leading, 15)
                                TextField("", text: $loginView.email)
                                    .autocapitalization(.none)
                                    .focused($emailFocused)
                                    .padding(.horizontal, 15)
                                    .font(.custom("SourceSansPro-SemiBold", size: 16))
                                    .foregroundColor(.black)
                            }
                        }
                        .frame(maxWidth: .infinity)
                        .frame(height: 52)
                        .background(.white)
                        .cornerRadius(30)
                        .padding(.horizontal, 40)
                        .padding(.top, 30)
                    }
                    HStack(spacing: 0) {
                        Image("Lock")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 24)
                            .padding(.leading, 15)
                        ZStack {
                            HStack {
                                if passwordFocused == false && password.isEmpty {
                                    Text("Password")
                                        .font(.custom("SourceSansPro-SemiBold", size: 16))
                                        .foregroundColor(.black)
                                }
                                Spacer()
                            }
                            .padding(.leading, 15)
                            TextField("", text: $password)
                                .focused($passwordFocused)
                                .padding(.horizontal, 15)
                                .font(.custom("SourceSansPro-SemiBold", size: 16))
                                .foregroundColor(.black)
                        }
                        Button {
                            //
                        } label: {
                            Image("Eye")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 16)
                        }
                        .padding(.trailing, 15)
                    }
                    .frame(maxWidth: .infinity)
                    .frame(height: 52)
                    .background(.white)
                    .cornerRadius(30)
                    .padding(.horizontal, 40)
                    .padding(.top, 30)
                    if login == false {
                        HStack(spacing: 0) {
                            Image("Lock")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 24)
                                .padding(.leading, 15)
                            ZStack {
                                HStack {
                                    if confirmPasswordFocused == false && confirmPassword.isEmpty {
                                        Text("Confirm password")
                                            .font(.custom("SourceSansPro-SemiBold", size: 16))
                                            .foregroundColor(.black)
                                    }
                                    Spacer()
                                }
                                .padding(.leading, 15)
                                TextField("", text: $confirmPassword)
                                    .focused($confirmPasswordFocused)
                                    .padding(.horizontal, 15)
                                    .font(.custom("SourceSansPro-SemiBold", size: 16))
                                    .foregroundColor(.black)
                            }
                            Button {
                                //
                            } label: {
                                Image("Eye")
                                    .resizable()
                                    .scaledToFit()
                                    .frame(width: 16)
                            }
                            .padding(.trailing, 15)
                        }
                        .frame(maxWidth: .infinity)
                        .frame(height: 52)
                        .background(.white)
                        .cornerRadius(30)
                        .padding(.horizontal, 40)
                        .padding(.top, 30)
                    }
                }
                .padding(.top, 50)
                VStack(spacing: 0) {
                    Button {
                        if !login {
                            UserDefaults.standard.set(loginView.email, forKey: "email")
                            connect.checkInternetConnection()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                if connect.isConnected {
                                    loginView.postSendCode {
                                        screen = "OTP"
                                    } error: {
                                        alert = true
                                    }
                                } else {
                                    loginView.loginError = "Отсутствует подключение к интернету"
                                    alert = true
                                }
                            }
                        }
                    } label: {
                        Text(login ? "Login" : "Signup")
                            .font(.custom("SourceSansPro-SemiBold", size: 16))
                            .foregroundColor(.white)
                            .padding(16)
                            .frame(maxWidth: .infinity)
                            .background(Color("Orange"))
                            .cornerRadius(100)
                            .padding(.horizontal, 40)
                    }
                    if login {
                        Text("Forget your password?")
                            .font(.custom("SourceSansPro-SemiBold", size: 14))
                            .foregroundColor(Color("TextDark"))
                            .padding(.top, 15)
                    } else {
                        Button {
                            self.check.toggle()
                        } label: {
                            HStack {
                                Image(systemName: check ? "checkmark.square" : "square")
                                Text("Accepting Terms of Services & Privacy policy")
                            }
                            .font(.custom("SourceSansPro-SemiBold", size: 14))
                            .foregroundColor(Color("TextDark"))
                        }
                        .padding(.top, 15)
                    }
                }
                .padding(.top, 50)
                Spacer()
                Spacer()
                VStack(spacing: 0) {
                    if login {
                        Text("Or Connect with")
                            .font(.custom("SourceSansPro-SemiBold", size: 12))
                            .foregroundColor(Color("TextDark"))
                        HStack(spacing: 15) {
                            Button {
                                //
                            } label: {
                                HStack(spacing: 15) {
                                    Image("Facebook")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 24)
                                    Text("Facebook")
                                }
                                .frame(maxWidth: .infinity)
                                .frame(height: 40)
                                .background(Color("Blue"))
                                .cornerRadius(30)
                            }
                            Button {
                                //
                            } label: {
                                HStack(spacing: 15) {
                                    Image("Twitter")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(height: 19)
                                    Text("Twitter")
                                }
                                .frame(maxWidth: .infinity)
                                .frame(height: 40)
                                .background(Color("BlueDark"))
                                .cornerRadius(30)
                            }
                        }
                        .foregroundColor(.white)
                        .font(.custom("SourceSansPro-SemiBold", size: 14))
                        .padding(.top, 30)
                        .padding(.horizontal, 32)
                    }
                    HStack {
                        Text(login ? "Dont’t have an account?" : "Already have account?")
                            .foregroundColor(Color("GrayWhite"))
                        Button {
                            withAnimation(.spring(response: 0.25, dampingFraction: 0.85, blendDuration: 0.25)) {
                                self.login.toggle()
                            }
                        } label: {
                            Text(login ? "Signup" : "Login")
                                .foregroundColor(Color("Orange"))
                        }
                    }
                    .font(.custom("SourceSansPro-SemiBold", size: 14))
                    .padding(.top, 30)
                }
                Spacer()
            }
        }
        .background(Color("BG"))
        .ignoresSafeArea(.keyboard)
        .alert(isPresented: $alert) {
            Alert(title: Text("Ошибка"), message: Text(loginView.loginError))
        }
    }
}
